
import sys
import ply.yacc as yacc
from Cparser import Cparser
from TreePrinter import TreePrinter
from TypeChecker import TypeChecker

if __name__ == '__main__':

    #filename = sys.argv[1] if len(sys.argv) > 1 else r"tests_err\label_test.in"
    filename = sys.argv[1] if len(sys.argv) > 1 else "example.txt"
    try:
        test_file = open(filename, "r")
    except IOError:
        print("Cannot open {0} file".format(filename))
        sys.exit(0)

    Cparser = Cparser()
    parser = yacc.yacc(module=Cparser)
    text = test_file.read()

    ast = parser.parse(text, lexer=Cparser.scanner)
    typeChecker = TypeChecker()
    typeChecker.visit(ast)
