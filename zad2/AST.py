class Node(object):
    def __str__(self):
        return self.printTree()


# sections : sections section | <empty> from new grammar
class Program(Node):
    def __init__(self, sections):
        self.sections = sections


class Declaration(Node):
    def __init__(self, type_name, inits):
        self.type_name = type_name
        self.inits = inits


class Init(Node):
    def __init__(self, identifier, expression):
        self.identifier = identifier
        self.expression = expression


# instructions

class PrintInstr(Node):
    def __init__(self, expressions):
        self.expressions = expressions


class LabeledInstr(Node):
    def __init__(self, identifier, instr):
        self.identifier = identifier
        self.instr = instr


class Assignment(Node):
    def __init__(self, identifier, expression):
        self.identifier = identifier
        self.expression = expression


class ChoiceInstr(Node):
    def __init__(self, condition, if_instr, else_instr=None):
        self.condition = condition
        self.if_instr = if_instr
        self.else_instr = else_instr


class WhileInstr(Node):
    def __init__(self, condition, instr):
        self.condition = condition
        self.instr = instr


class RepeatInstr(Node):
    def __init__(self, instrs, condition):
        self.condition = condition
        self.instrs = instrs


class ReturnInstr(Node):
    def __init__(self, expression):
        self.expression = expression


class ContinueInstr(Node):
    def __init__(self):
        pass


class BreakInstr(Node):
    def __init__(self):
        pass


# compound_instr : '{' statements '}' from new grammar
class CompoundInstr(Node):
    def __init__(self, statements):
        self.statements = statements


# fundef and expressions

class FunDef(Node):
    def __init__(self, return_type, name, args, compound_instr):
        self.return_type = return_type
        self.name = name
        self.args = args
        self.compound_instr = compound_instr


class Arg(Node):
    def __init__(self, arg_type, arg):
        self.arg_type = arg_type
        self.arg = arg


# expression : ID '(' expr_list_or_empty ')'
class FunCall(Node):
    def __init__(self, name, args):
        self.name = name
        self.args = args


class Identifier(Node):
    def __init__(self, identifier):
        self.identifier = identifier


class BinExpr(Node):
    def __init__(self, op, left, right):
        self.op = op
        self.left = left
        self.right = right


# Consts

class Const(Node):
    def __init__(self, value):
        self.value = value
