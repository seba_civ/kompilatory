import os
import sys
import re
import codecs

phrase_regex = re.compile(r'([\w \-+*:,;]+)([\.?!]+|$)')
shortcut_pattern = r'\b[a-zA-Z]{1,3}\.'
shortcut_regex = re.compile(shortcut_pattern)
float_pattern = r'(\b[-+]?(\d+\.\d*|\.\d+)(?:(e|E)[-+]?\d+)?)\s'
float_regex = re.compile(float_pattern)
email_pattern = r'(\b\w+@\w+(:?\.\w+)+\b)'
email_regex = re.compile(email_pattern)
integer_pattern = r'(0|-32768|[\s-](3276[0-7]|327[0-5]\d|32[0-6]\d{2}|3[0-1]\d{3}|[1-2]\d{4}|[1-9]\d{1,3}|' \
                  r'[1-9]))(\s|[\.,:;!?]\s)'
integer_regex = re.compile(integer_pattern)
european_pattern = r'(?P<Ayear>\d{4})(?P<Asep>[-\./])' \
                   r'((?P<A31M>0[13578]|1[02])(?P=Asep)(?P<A31D>[0-2]\d|3[0-1])|' \
                   r'((?P<A30M>0[469]|11)(?P=Asep)(?P<A30D>[0-2]\d|30))|' \
                   r'((?P<A29M>02)(?P=Asep)(?P<A29D>[0-1]\d|2[0-9])))'
american_pattern = r'((?P<E31D>[0-2]\d|3[0-1])(?P<Esep1>[-\./])(?P<E31M>0[13578]|1[02])|' \
                   r'(?P<E30D>[0-2]\d|30)(?P<Esep2>[-\./])(?P<E30M>0[469]|11)|' \
                   r'(?P<E29D>[0-1]\d|2[0-9])(?P<Esep3>[-\./])(?P<E29M>02))' \
                   r'((?P=Esep1)|(?P=Esep2)|(?P=Esep3))(?P<Eyear>\d{4})'
data_regex = re.compile(european_pattern + '|' + american_pattern)


def get_meta(content):
    reg = re.compile('^<META NAME=".+$', re.MULTILINE)
    return "\n".join(reg.findall(content))


def get_article(content):
    reg = re.compile(r'(?<=<P>)[\s\S]*?(?=<META)', re.MULTILINE)
    r = reg.findall(content)
    return ''.join(r)


def unique_matches(match):
    if match:
        s = set()
        for x in match:
            s.add(x)
        return len(s)
    else:
        return 0


def unique_floats(match):
    if match:
        s = set()
        for x in match:
            try:
                s.add(float(x[0]))
            except:
                pass
        return len(s)
    else:
        return 0


def unique_integers(match):
    if match:
        s = set()
        for x in match:
            try:
                s.add(int(x[0]))
            except:
                pass
        return len(s)
    else:
        return 0


def processFile(filepath):
    fp = codecs.open(filepath, 'rU', 'iso-8859-2')

    content = fp.read()

    meta = get_meta(content)
    author = re.compile('<META NAME="AUTOR" CONTENT="(.+)">', re.MULTILINE).findall(meta)
    genre = re.compile('<META NAME="DZIAL" CONTENT=".+/(.+)">', re.MULTILINE).findall(meta)
    keywords = ", ".join(re.compile(r'<META NAME="KLUCZOWE_\d" CONTENT="(.+)">', re.MULTILINE).findall(meta))

    article = get_article(content)

    shortcuts = shortcut_regex.findall(article)
    #print shortcuts
    floats = float_regex.findall(article)
    #print floats
    emails = email_regex.findall(article)
    #print emails
    integers = integer_regex.findall(article)
    datas = len(data_regex.findall(article))

    cleared_content = re.sub(float_pattern, '', re.sub(email_pattern, '', re.sub(shortcut_pattern, '', re.sub(american_pattern, '', re.sub(european_pattern, '', article)))))
    phrases = phrase_regex.findall(cleared_content)
    #print phrases

    fp.close()
    print("nazwa pliku:", filepath)
    print("autor:", author[0])
    print("dzial:", genre[0])
    print("slowa kluczowe:", keywords)
    print("liczba zdan:", len(phrases))
    print("liczba skrotow:", unique_matches(shortcuts))
    print("liczba liczb calkowitych z zakresu int:", unique_integers(integers))
    print("liczba liczb zmiennoprzecinkowych:", unique_floats(floats))
    print("liczba dat:", datas)
    print("liczba adresow email:", unique_matches(emails))
    print("\n")

    #print(article)



try:
    path = sys.argv[1]
except IndexError:
    print("Brak podanej nazwy katalogu")
    sys.exit(0)


tree = os.walk(path)

for root, dirs, files in tree:
    for f in files:
        if f.endswith(".html"):
            filepath = os.path.join(root, f)
            processFile(filepath)