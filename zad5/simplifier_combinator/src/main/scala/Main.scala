
import java.io.FileReader
import java.io.FileNotFoundException
import java.io.IOException

import AST.{Node, NodeList}
import simplifier.Simplifier

object Main {

  def main(args: Array[String]) {
    if (args.length == 0) {
      println("Usage: sbt \"run filename ...\"")
      return
    }

    val parser = new Parser()

    for (arg <- args) {
      try {
        println(s"Parsing file: $arg")
        val reader = new FileReader(arg)
        val parseResult = parser.parseAll(reader)
        parseResult match {
          case parser.Success(result: List[Node], in) =>
            println("\nAST:")
            println(parseResult.get)
            val tree = NodeList(result)
            println("\nProgram before optimization:")
            println(tree.toStr)

            val simplifiedTree = Simplifier.simplify(tree)
            println("\nAST after optimization:")
            println(simplifiedTree)
            println("\nProgram after optimization:")
            println(simplifiedTree.toStr)
          case parser.NoSuccess(msg: String, in) => println(s"FAILURE $parseResult")
        }
      } catch {
        case ex: FileNotFoundException => println(s"Couldn't open file $arg")
        case ex: IOException => println(s"Couldn't read file $arg")
      }
    }
  }
}
