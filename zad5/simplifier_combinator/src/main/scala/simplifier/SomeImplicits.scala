package simplifier

import AST._

import scala.language.implicitConversions

/**
  * Created by Sebastian on 2017-01-07.
  */

object SomeImplicits {
  implicit def numToInt(node: IntNum): Int = node.value
  implicit def numToDouble(node: FloatNum): Double = node.value
  implicit def intToNum(value: Int): IntNum = IntNum(value)
  implicit def doubleToNum(value: Double): FloatNum = FloatNum(value)
  implicit def boolToNode(value: Boolean): Node = if (value) TrueConst else FalseConst

  object One {
    def unapply(arg: Node): Boolean = arg match {
      case IntNum(1) => true
      case FloatNum(1.0) => true
      case _ => false
    }
  }

  object Zero {
    def unapply(arg: Node): Boolean = arg match {
      case IntNum(0) => true
      case FloatNum(0.0) => true
      case _ => false
    }
  }

  implicit class RichNode(private val node: Node) extends AnyVal {
    def ===(other: Node): Boolean = {
      (node, other) match {
        case (expr1: BinExpr, expr2: BinExpr) if bothCommutative(expr1.op, expr2.op) =>
          (expr1.left === expr2.left && expr1.right === expr2.right) || (expr1.left === expr2.right && expr1.right === expr2.left)
        case _ => node == other
      }
    }

    private def bothCommutative(op1: String, op2: String): Boolean = {
      op1 == op2 && isCommutative(op1) && isCommutative(op2)
    }

    private def isCommutative(operator: String): Boolean = operator match {
      case "+" | "*" | "and" | "or" => true
      case _ => false
    }
  }
}
