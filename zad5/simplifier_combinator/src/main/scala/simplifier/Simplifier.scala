package simplifier

import AST._
import SomeImplicits._

object Simplifier {

  private val OperatorNegation = Map(
    "==" -> "!=",
    "!=" -> "==",
    "<=" -> ">",
    ">=" -> "<",
    "<" -> ">=",
    ">" -> "<="
  )

  // Intellij LIES, it's working :)
  private def eval[T : ExtendedNumeric](op: String)(a: T, b: T): Node = {
    val ev = implicitly[ExtendedNumeric[T]]
    import ev.{mkExtendedOps, mkOrderingOps, toNode}
    op match {
      case "+" => a + b
      case "-" => a - b
      case "*" => a * b
      case "/" => a / b
      case "**" => a ** b
      case "<" => a < b
      case "<=" => a <= b
      case ">" => a > b
      case ">=" => a >= b
    }
  }

  def simplify(node: Node): Node = node match {
    case u: Unary => simplifyUnary(u)
    case e: IfElseExpr => simplifyIfElseExpr(e)
    case e: IfElseInstr => simplifyIfElseInstr(e)
    case e: ElifInstr => simplifyElifInstr(e)
    case w: WhileInstr => simplifyWhile(w)
    case e: BinExpr => simplifyBinExpr.apply(e.copy(left = simplify(e.left), right = simplify(e.right)))
    case KeyDatumList(list) =>
      KeyDatumList(list.iterator.map(simplifyKeyDatum).map(n => n.key -> n).toMap.toList.map({case (_, n) => n}))

    case Assignment(left, right) if left === right => EmptyInstr
    case a: Assignment => a.copy(right = simplify(a.right))

    case NodeList(list) =>
      simplifyList(list).iterator.filterNot(n => n == EmptyInstr).toList match {
        case Nil => EmptyInstr
        case head :: Nil => head
        case nodes => removeDeadAssignments(nodes)
      }
    case _ => node
  }

  private def removeDeadAssignments(nodes: List[Node]): Node = {
    val list = nodes.iterator.zip(nodes.tail.iterator ++ Iterator(EmptyInstr))
      .filterNot({
        case (a: Assignment, b: Assignment) => a.left === b.left
        case _ => false
      })
      .map({ case (n, _) => n }).toList
    list match {
      case head :: Nil => head
      case n => NodeList(n)
    }
  }

  private def concat(first: List[Node], second: List[Node]): List[Node] = simplifyList(first) ++ simplifyList(second)

  def simplifyList(list: List[Node]): List[Node] = list.iterator.map(simplify).toList

  def simplifyKeyDatum(keyDatum: KeyDatum): KeyDatum = KeyDatum(simplify(keyDatum.key), simplify(keyDatum.value))

  def simplifyWhile(wh: WhileInstr): Node = wh.copy(cond = simplify(wh.cond)) match {
    case WhileInstr(FalseConst, body) => EmptyInstr
    case w => w.copy(body = simplify(w.body))
  }

  def simplifyIfElseInstr(expr: IfElseInstr): Node = expr.copy(cond = simplify(expr.cond)) match {
    case IfElseInstr(TrueConst, left, _) => simplify(left)
    case IfElseInstr(FalseConst, _, right) => simplify(right)
    case e => e.copy(left = simplify(e.left), right = simplify(e.right))
  }

  def simplifyElifInstr(expr: ElifInstr): Node = {
    def simplifyClause(clause: ConditionalClause): ConditionalClause = {
      clause.copy(suite = simplify(clause.suite))
    }

    val clauseList = (Iterator(expr.ifClause) ++  expr.elifClauses.iterator)
      .map(_.cond)
      .map(simplify)
      .zip(Iterator(expr.ifClause) ++  expr.elifClauses.iterator)
      .filterNot({case (n, _) => n == FalseConst})
      .map({case (n, c) => c.copy(cond = n)})
      .toList
    if (clauseList.isEmpty) {
      expr.elseClause.map(simplify).getOrElse(EmptyInstr)
    } else if (clauseList.size == 1) {
      if (clauseList.head.cond == TrueConst) {
        simplify(clauseList.head.suite)
      } else {
        expr.elseClause.map(simplify).map(s => IfElseInstr(clauseList.head.cond, simplify(clauseList.head.suite), s))
          .getOrElse(IfInstr(clauseList.head.cond, simplify(clauseList.head.suite)))
      }
    } else {
      expr.copy(ifClause = simplifyClause(clauseList.head), elifClauses = clauseList.tail.map(simplifyClause), elseClause = expr.elseClause.map(simplify))
    }
  }

  def simplifyIfElseExpr(expr: IfElseExpr): Node = expr.copy(cond = simplify(expr.cond)) match {
    case IfElseExpr(TrueConst, left, _) => simplify(left)
    case IfElseExpr(FalseConst, _, right) => simplify(right)
    case e => e.copy(left = simplify(e.left), right = simplify(e.right))
  }

  def simplifyUnary(unary: Unary): Node = unary.copy(expr = simplify(unary.expr)) match {
    case Unary("not", FalseConst) => TrueConst
    case Unary("not", TrueConst) => FalseConst
    case Unary("not", Unary("not", right)) => simplify(right)
    case Unary("-", Unary("-", right)) => simplify(right)
    case Unary("not", BinExpr(op, left, right)) if OperatorNegation.contains(op) => BinExpr(OperatorNegation(op), left, right)
    case e => e
  }

  private def simplifyConstants: PartialFunction[BinExpr, Node] = {
    case BinExpr(op, left: IntNum, right: IntNum) => eval(op)(left.value, right.value)
    case BinExpr(op, left: FloatNum, right: FloatNum) => eval(op)(left.value, right.value)
    case BinExpr(op, left: IntNum, right: FloatNum) => eval(op)(left.value.toDouble, right.value)
    case BinExpr(op, left: FloatNum, right: IntNum) => eval(op)(left.value, right.value.toDouble)
  }

  private def simplifyBoolean: PartialFunction[BinExpr, Node] = {
    case BinExpr("or", FalseConst, right) => right
    case BinExpr("or", left, FalseConst) => left
    case BinExpr("or", TrueConst, right) => TrueConst
    case BinExpr("or", left, TrueConst) => TrueConst
    case BinExpr("and", FalseConst, right) => FalseConst
    case BinExpr("and", right, FalseConst) => FalseConst
    case BinExpr("and", TrueConst, right) => right
    case BinExpr("and", left, TrueConst) => left
    case BinExpr("or" | "and", left, right) if left === right => left
  }

  private def simplifyBinExpr: PartialFunction[BinExpr, Node] = simplifyConstants orElse simplifyBoolean orElse {
    case BinExpr("+", Tuple(first), Tuple(second)) => Tuple(concat(first, second))
    case BinExpr("+", ElemList(first), ElemList(second)) => ElemList(concat(first, second))

    case BinExpr(("*" | "/"), left@Zero(), _) => left
    case BinExpr("*", _, right@Zero()) => right
    case BinExpr(("*" | "/"), left, One()) => left
    case BinExpr("*", One(), right) => right
    case BinExpr("*", left, BinExpr("/", One(), right)) => simplify(BinExpr("/", left, right))
    case BinExpr("/", left, BinExpr("/", One(), right)) => simplify(BinExpr("*", left, right))
    case BinExpr("/", One(), BinExpr("/", left, right)) => simplify(BinExpr("/", right, left))

    case BinExpr(("+" | "-"), left, Zero())  => left
    case BinExpr("+", Zero(), right) => right
    case BinExpr("-", Zero(), right) => simplifyUnary(Unary("-", right))

    case BinExpr("**", left, Zero()) => 1
    case BinExpr("**", left, One()) => left
    case BinExpr("*", BinExpr("**", left1, right1), BinExpr("**", left2, right2)) if left1 === left2 =>
      BinExpr("**", left1, simplify(BinExpr("+", right1, right2)))
    case BinExpr("**", BinExpr("**", left1, right1), right) => BinExpr("**", left1, simplify(BinExpr("*", right1, right)))

    case BinExpr("/", left, right) if left === right => 1
    case BinExpr("-", left, right) if left === right => 0

    case BinExpr("==" | ">=" | "<=", left, right) if left === right => TrueConst
    case BinExpr("!=" | ">" | "<", left, right) if left === right => FalseConst

    case b => simplifyCommutative(b)
  }

  private def simplifyCommutative(binExpr: BinExpr): Node = {
    def changed(left: Node, v: Node): Node = if (v === left) binExpr else v

    binExpr match {
      case BinExpr("+", Unary("-", expr), right) if right === expr => 0
      case BinExpr("+", leftExpr, rightExpr) =>
        def rec(node: Node): Node = {
          node match {
            case BinExpr("-", left, right) if rightExpr === right => left
            case BinExpr("-", Unary("-", expr), right) if rightExpr === expr => Unary("-", right)
            case BinExpr("+", Unary("-", expr), right) if rightExpr === expr => right
            case e@BinExpr("-" | "+", left , _) => e.copy(left = rec(left))
            case _ => node
          }
        }

        changed(leftExpr, rec(leftExpr))

      case BinExpr("-", leftExpr, rightExpr) =>
        def rec(node: Node): Node = {
          node match {
            case BinExpr("+", left, right) if rightExpr === right => left
            case BinExpr("+", left, right) if rightExpr === left => right
            case BinExpr("-", left, right) if rightExpr === left => Unary("-", right)
            case e@BinExpr("-" | "+", left , _) => e.copy(left = rec(left))
            case _ => node
          }
        }

        changed(leftExpr, rec(leftExpr))

      case _ => binExpr
    }
  }
}
