package simplifier

import AST.{FloatNum, IntNum, Node}

import scala.math.Numeric.{DoubleIsFractional, IntIsIntegral}
import scala.math.Ordering
import scala.language.implicitConversions

/**
  * Created by Sebastian on 2017-01-07.
  */

object ExtendedNumeric {
  implicit object IntNumericEvidence extends ExtendedNumeric[Int] with IntIsIntegral with Ordering.IntOrdering {
    override def raise(x: Int, y: Int): Int = {
      y match {
        case 0 => 1
        case 1 => x
        case _ if y%2 == 0 => raise(x*x, y/2)
        case _ => x * raise(x*x, (y-1)/2)
      }
    }
    override def div(x: Int, y: Int): Int = super.quot(x, y)
    override implicit def toNode(value: Int): Node = IntNum(value)
  }

  implicit object FloatNumericEvidence extends ExtendedNumeric[Double] with DoubleIsFractional with Ordering.DoubleOrdering {
    override def raise(x: Double, y: Double): Double = scala.math.pow(x, y)
    override implicit def toNode(value: Double): Node = FloatNum(value)
  }
}

trait ExtendedNumeric[T] extends Numeric[T] {
  def div(x: T, y: T): T
  def raise(x: T, y: T): T

  class ExtendedOps(lhs: T) extends Ops(lhs) {
    def /(rhs: T) = div(lhs, rhs)
    def **(rhs: T) = raise(lhs, rhs)
  }

  implicit def mkExtendedOps(lhs: T): ExtendedOps = new ExtendedOps(lhs)
  implicit def toNode(value: T): Node
}
