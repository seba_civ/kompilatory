
import scala.util.parsing.combinator._
import scala.util.parsing.input.Positional
import AST._

class Parser extends JavaTokenParsers {

  val precedenceList: List[List[String]] = List(
    List("is", ">=", "<=", "==", "!=", "<", ">"), // order matters also within inner list, longer op should go before shorter one, e.g. "<=" before "<", if one is a prefix of another
    List("+", "-"),
    List("*", "/", "%"),
    List("**")
  )

  val minPrec = 0
  val maxPrec = precedenceList.length - 1

  protected override val whiteSpace = """(\t|\r|[ ]|#.*|(?s)/\*.*?\*/)+""".r

  val reserved: Parser[String] =
    "and\\b".r |
    "class\\b".r |
    "def\\b".r |
    "else\\b".r |
    "False\\b".r |
    "if\\b".r |
    "is\\b".r |
    "input\\b".r |
    "lambda\\b".r |
    "not\\b".r |
    "or\\b".r |
    "print\\b".r |
    "return\\b".r |
    "True\\b".r |
    "while\\b".r |
    "elif\\b".r

  val identifier: Parser[String] = not(reserved) ~> """[a-zA-Z_]\w*""".r

  val newl: Parser[Node] = """(\r?\n)|\r""".r ^^ StringConst

  val lpar = rep1(newl) ~ "{" ~ rep(newl)
  val rpar = rep(newl) ~ "}" ~ rep1(newl)

  val floatLiteral: Parser[Double] =
    """\d+(\.\d*)|\.\d+""".r ^^ {
      _.toDouble
    }

  val intLiteral: Parser[Int] =
    """\d+""".r ^^ {
      _.toInt
    }

  def const: Parser[Node] = (
    floatLiteral ^^ FloatNum
      | intLiteral ^^ IntNum
      | stringLiteral ^^ StringConst
      | "True" ^^^ TrueConst
      | "False" ^^^ FalseConst
    )

  def parseAll(input: java.io.FileReader): ParseResult[List[Node]] = parseAll(program, input)

  // stands for def program: Parser[List[Node]] = rep(statement|newl)
  def program: Parser[List[Node]] = rep(newl) ~> rep(statement <~ rep(newl))


  def statement: Parser[Node] = (
    simple_statement
      | compound_statement
    )


  def expression: Parser[Node] = (
    ("lambda" ~> id_list <~ ":") ~ expression ^^ {
      case formal_args ~ body => LambdaDef(IdList(formal_args), body)
    }
      | or_expr
    )


  def or_expr: Parser[Node] = rep1sep(and_expr, "or") ^^ (xs => (xs.head /: xs.tail) (BinExpr("or", _, _)))

  def and_expr: Parser[Node] = rep1sep(not_expr, "and") ^^ (xs => (xs.head /: xs.tail) (BinExpr("and", _, _)))


  def not_expr: Parser[Node] = (
    "not" ~> not_expr ^^ (Unary("not", _)) // equivalent to "not"~>not_expr ^^ { case not_expr => Unary("not", not_expr) }
      | binary(minPrec) ~ opt(("if" ~> binary(minPrec) <~ "else") ~ expression) ^^ {
      case left ~ None => left
      case left ~ Some(cond ~ right) => IfElseExpr(cond, left, right)
    }
    )

  def target: Parser[Node] = (
    identifier ^^ Variable
      | subscription
      | get_attr
    )

  def subscription: Parser[Node] = (expression <~ "[") ~ expression <~ "]" ^^ {
    case expr ~ sub => Subscription(expr, sub)
  }

  def get_attr: Parser[Node] = (expression <~ ".") ~ identifier ^^ {
    case expression ~ id => GetAttr(expression, id)
  }


  def binary(level: Int): Parser[Node] = {
    if (level > maxPrec) unary
    else if (level == maxPrec) rep1sep(binary(level + 1), "**") ^^ { _.reduceRight(BinExpr("**", _, _)) }
    else chainl1(binary(level + 1), binaryOp(level)) // equivalent to binary(level+1) * binaryOp(level)
  }

  // operator precedence parsing takes place here
  def binaryOp(level: Int): Parser[((Node, Node) => BinExpr)] = {
    precedenceList(level).map {
      op => op ^^^ { (a: Node, b: Node) => BinExpr(op, a, b) }
    }.reduce((head, tail) => head | tail)
  }


  def unary: Parser[Node] = (
    ("+" | "-") ~ unary ^^ {
      case "+" ~ expression => expression
      case "-" ~ expression => Unary("-", expression)
    }
      | primary
    )


  def primary: Parser[Node] = (
    lvalue
      | const
      | "(" ~> expression <~ ")"
      | "[" ~> expr_list_comma <~ "]" ^^ {
      case NodeList(x) => ElemList(x)
      case node =>
        println("Warn: expr_list_comma didn't return NodeList")
        node
    }
      | "{" ~> key_datum_list <~ "}"
      | tuple
    )


  def lvalue: Parser[Node] = identifier ~ trailer ^^ {
    case i ~ list => foldTrailer(Variable(i), list)
  }

  def trailer: Parser[List[(String, Node)]] = rep(
    "(" ~> expr_list <~ ")" ^^ (expr_list => "(" -> expr_list)
      | "[" ~> expression <~ "]" ^^ (expression => "[" -> expression)
      | "." ~> identifier ^^ (id => "." ->  Variable(id))
  )

  def foldTrailer(head: Node, list: List[(String, Node)]): Node = {
    list match {
      case (".", Variable(id)) :: tail => foldTrailer(GetAttr(head, id), tail)
      case ("(", attr) :: tail => foldTrailer(FunCall(head, attr), tail)
      case ("[", attr) :: tail => foldTrailer(Subscription(head, attr), tail)
      case _ => head
    }
  }

  def expr_list_comma: Parser[Node] = expr_list <~ opt(",")

  //def expr_list: Parser[Node] = repsep(expression, ",") ^^ NodeList // repsep returns List[Node]
  def expr_list: Parser[NodeList] = repsep(expression, ",") ^^ NodeList // repsep returns List[Node]

  //def key_datum_list: Parser[Node] = repsep(key_datum, ",") ^^ KeyDatumList
  def key_datum_list: Parser[KeyDatumList] = repsep(key_datum, ",") ^^ KeyDatumList

  def key_datum: Parser[KeyDatum] = expression ~ ":" ~ expression ^^ {
    case key ~ ":" ~ value => KeyDatum(key, value)
  }

  def funcdef: Parser[FunDef] = ("def" ~> identifier <~ "(") ~ id_list ~ ((")" ~ ":") ~> suite) ^^ {
    case id ~ id_list ~ suite => FunDef(id, IdList(id_list), suite)
  }

  def classdef: Parser[ClassDef] = ("class" ~> identifier) ~ ("(" ~> expr_list <~ ")").? ~ (":" ~> suite) ^^ {
    case id ~ Some(expr_list) ~ suite => ClassDef(id, expr_list, suite)
    case id ~ None ~ suite => ClassDef(id, NodeList(List()), suite)
  }


  def id_list: Parser[List[Variable]] = repsep(identifier, ",") ^^ (_.map(Variable))

  def suite: Parser[Node] = lpar ~> statement_list <~ rpar

  def statement_list: Parser[Node] = rep1(statement) ^^ NodeList

  def simple_statement: Parser[Node] = (small_statement_list <~ ';'.?) <~ rep1(newl) ^^ NodeList

  def small_statement: Parser[Node] = (
    print_instr
      | return_instr
      | assignment
      | expression
    )

  def small_statement_list: Parser[List[Node]] = rep1sep(small_statement, ";")

  def compound_statement: Parser[Node] = (
    if_else_stmt
      | while_stmt
      | funcdef
      | classdef
    )

  def print_instr: Parser[PrintInstr] = "print" ~> expression ^^ PrintInstr

  def return_instr: Parser[ReturnInstr] = "return" ~> expression ^^ ReturnInstr

  def assignment: Parser[Assignment] = (target <~ "=") ~ expression ^^ {
    case target ~ expression => Assignment(target, expression)
  }

  def if_else_stmt: Parser[Node] = {
    "if" ~> expression ~ (":" ~> suite) ~ ("elif" ~> expression ~ (":" ~> suite)).* ~ ("else" ~ ":" ~> suite).? ^^ {
      case ifCond ~ ifSuite ~ Nil ~ None => IfInstr(ifCond, ifSuite)
      case ifCond ~ ifSuite ~ Nil ~ Some(elseSuite) => IfElseInstr(ifCond, ifSuite, elseSuite)
      case ifCond ~ ifSuite ~ elif ~ elseSuite =>
        val elifClauses = elif.iterator.map(p => ConditionalClause(p._1, p._2)).toList
        ElifInstr(ConditionalClause(ifCond, ifSuite), elifClauses, elseSuite)
    }
  }

  def while_stmt: Parser[WhileInstr] = "while" ~> expression ~ (":" ~> suite) ^^ {
    case expression ~ suite => WhileInstr(expression, suite)
  }

  def input_instr: Parser[InputInstr] = "input" ~ "(" ~ ")" ^^^ InputInstr()

  def tuple: Parser[Tuple] = "(" ~> tuple_list <~ ")" ^^ (tuple_list => Tuple(tuple_list))

  def tuple_list: Parser[List[Node]] = rep1sep(expression, ",")
}
